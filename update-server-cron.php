<?php

	//Run this cron job once a minute e.g.
	//* * * * *	/usr/bin/php /atomjump-server-path/plugins/emoticons_large/update-server-cron.php
	
	//AND
	//Ensure there is a folder called 'update', which is 'chmod 777 update'

	//Check for an update trigger file
	if(file_exists(__DIR__ . "/update/update-icons.txt")) {
		//Delete the file
		unlink(__DIR__ . "/update/update-icons.txt");
		
		//And do a server-side git pull
		chdir(__DIR__);
		$return = shell_exec("sudo git pull");
		
		echo "Git pull run. Result: " . $return . "\n";
	} else {
		echo "No update has been requested.\n";
	}

?>
