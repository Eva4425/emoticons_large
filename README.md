<img src="https://atomjump.com/images/logo80.png">

# emoticons_large
Large emoticons (full size images) for the AtomJump Messaging Server


## Requirements

AtomJump Messaging Server >= 3.2.3

## Installation

Unzip or git clone into the folder: your-messaging-server/plugins/emoticons_large

```
 cd your-messaging-server/plugins/
 git clone https://src.atomjump.com/atomjump/emoticons_large.git
 cd emoticons_large/config
 cp configORIGINAL.json config.json
 nano config.json
```

Set the 	

```javascript
	"serverPath": "/your/server/path/here/",	//path to your AtomJump Messaging server
	"staging" : true							//true for staging, false for production version of config
```

Add the string "emoticons_large" into your-loop-server/config/config.json plugins array to enable the plugin. e.g. 

     "plugins": [
         "emoticons_large"
      ]
      
      
## Using

Tap on the 'Upload Icon' at the bottom of any Atomjump popup. Scroll down the page to find the available emoticons.
Tap on an emoticon for a new message with that emoticon.

You are free to add further emoticon sets into the 'icons' folder. They should be put into a named subdirectory, which
will automatically be read. Images can be either .jpg or .png.


## Modifying the icons

See the file icons/basic/include.json for some instructions on changing the displayed icons.

For an automatic project update button on the server, change the main AtomJump Messaging server config.json 'usingStaging' parameter to be : true, and the config/config.json config "staging" in this project to be : true.

Configure the the cron job once a minute e.g.
```
* * * * *	/usr/bin/php /atomjump-server-path/plugins/emoticons_large/update-server-cron.php
```

And ensure there is a folder called 'update'
```
cd /atomjump-server-path/plugins/emoticons_large/
sudo mkdir update
sudo chmod 777 update
```

Then tap the icons, and look for the black 'download' icon. Tap that after a project update, and it will update the icon set on the server within one minute.

## Credits
Most of these icons are from Pixabay.com.

But here are some others:
https://www.veryicon.com/icons/business/sunshine/home-191.html
https://www.veryicon.com/icons/internet--web/iview-3-x-icons/md-folder-open.html
https://www.veryicon.com/icons/object/material-design-icons-1/folder-download-21.html



